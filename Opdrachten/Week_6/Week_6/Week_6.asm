; demoprogramma 6_b: 16 bits optelling met opslag in data memory
;
.INCLUDE "m128def.inc"
;
.CSEG
.ORG 	$0000
		rjmp main
;
.CSEG
.ORG	$0200
;
main:
; initialisatie data geheugen
;						
			LDI		YH, $04		; Y <- $0400		adres in data memory
			LDI		YL, $00		;
		;	LDI		YH, HIGH(a)	;
		;	LDI		YL, LOW(a)	;

			LDD		R8, Y+0			; Haal waarden op uit data memory
			LDD		R9, Y+1			;
			LDD		R6, Y+2			;
			LDD		R7, Y+3			;

									; 16 bits optelling: R5/R4 = R9/R8 + R7/R6  (23 ab + 44 98 = 68 43)
									; Stap 1: 	tel de LSB�fs op: R4 = R6+R8 
			MOV		R4, R8			; 		R4 <- R8 		
			ADD		R4, R6			; 		R4 <- R4+R6 		 misschien Carry gezet 
									; Stap 2: 	tel de MSB�fs op met 
									;				de (eventuele) carry uit LSB�fs:
									;				R3 = R5+R6+C
			MOV		R5, R9			; 		R5 <- R9 
			ADC		R5, R7			; 		R5 <- R5+R7+C 
									;
									; Opslag resultaat
									;
			STD		Y+4,R4			; Sla resultaat op in data memory
			STD		Y+5,R5			; 
		
		no_end:   					; unlimited loop, when done
		rjmp no_end			
		

.DSEG
.ORG	$0400
	a:	.BYTE	2
	b:	.BYTE	2
	c:	.BYTE	2
