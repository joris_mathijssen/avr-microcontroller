#include <avr/io.h>
#include <util/delay.h>

	//Borrowed Wait method
	void wait( int ms )  {
		for (int i=0; i<ms; i++){
			_delay_ms( 1 );	
		}	
	}
	//Modification of Blinky:
	int main( void )  {
		DDRD = 0b11111111; //Set all pins to output on portD
		PORTD = 0x80; //Start on pin 7
		while (1)  {
			PORTD = (PORTD >> 1); //Shift to 6			
			wait( 5000 );
			PORTD = (PORTD << 1); //Shift to 7					
			wait( 5000 );
		}
		return 1;
	}
