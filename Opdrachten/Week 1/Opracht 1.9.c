/* Project name:	LedSnake
 * Author: 		Avans-TI, Julian G. West
 * Revision History: 
     			20140130: - initial release;
 * Description:	LED light snake running over PORTA's LEDs then PORTB's.
 * Test configuration:
     MCU:      	ATmega128
     Dev.Board: 	BIGAVR6
     Oscillator:	External Clock 08.0000 MHz
     Ext. Modules:    -
     SW:       	AVR-GCC
 * NOTES:
     - Turn ON the PORT LEDs at SW12.1 - SW12.8
*/

#include <avr/io.h>
#include <util/delay.h>

//Borrowed wait method
void wait( int ms )
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );
	}
}

// main program
int main (void)
{ 
	DDRA=0xFF;					// PORTA is output
	DDRB=0xFF;					// PORTB too.
	
 	while (1) 
  	{ 						// DO forever 
		for (int j=0x01; j<= 0x80; j<<=1)
		{ 					// blink LED 0,1,2,3,4,5,6,7 
			PORTA = j;			//  j to output
			wait( 400 );			// wait 400ms (Not the advised 25, for logic)
		} 
		PORTA = 0x00;
		PORTD = 0x00;
		for (int j=0x80; j>= 0x01; j>>=1)
		{ 					// blink LED 7,6,5,4,3,2,1,0 
			PORTB = j; 			//  j to output	
			wait( 400 ); 			// wait 400 ms
		} 
		PORTB = 0x00;
		
		
	}// end while
	return 1;
 } // end main 
