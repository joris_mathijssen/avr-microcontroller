/* Project name:	Running_LightEdited
 * Author: 		Avans-TI, WvdE
 * Revision History: 
     			20110124: - initial release;
				20140130: - Minor functionality edit
 * Description:	LED running light with PortA of the BIGAVR6 board, skips an LED
 * Test configuration:
     MCU:      	ATmega128
     Dev.Board: 	BIGAVR6
     Oscillator:	External Clock 08.0000 MHz
     Ext. Modules:    -
     SW:       	AVR-GCC
 * NOTES:
     - Turn ON the PORT LEDs at SW12.1 - SW12.8
*/

#include <avr/io.h>
#include <util/delay.h>

// wait():    busy waiting for 'ms' millisecond
// Used library: util/delay.h
void wait( int ms )
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );			// library function (max 30 ms at 8MHz)
	}
}

// main program
int main (void)
{ 
	DDRA=0xFF;					// PORTA is output
 	while (1) 
  	{ 						// DO forever 
		for (int j=0x01; j< 0x80; j<<=2) //EDIT: Upped to 2 for LED skipping
		{ 					// blink LED 0,1,2,3,4,5,6,7 
			PORTA = j;			//  j to output
			wait( 5000 );			// wait 200 ms; debugging: wait(2) 
		} 
		for (int j=0x80; j> 0x01; j>>=2) //EDIT: Upped to 2 for LED skipping
		{ 					// blink LED 7,6,5,4,3,2,1,0 
			PORTA = j; 			//  j to output		 
			wait( 5000 ); 			// wait 200 ms; debugging: wait(2)
		} 
	}// end while
	return 1;
 } // end main 
