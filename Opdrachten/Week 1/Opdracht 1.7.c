#include <avr/io.h>
#include <util/delay.h>

	//Borrowed Wait method
	void wait( int ms )  {
		for (int i=0; i<ms; i++){
			_delay_ms( 1 );	
		}	
	}
	//Modification of Blinky:
	//This version keeps the LED enabled.
	int main( void )  {
		DDRC = 0b00000000;
		DDRD = 0b11111111; //Set all pins to output on portD
		while (1)  {
			if(PINC != 1){ //When port C bit 1 not high
				PORTD ^= 0x80; //Toggle 7		
				wait( 5000 ); 
			}
			else
			{
				PORTD ^= 0x40;//Toggle 6
				wait( 5000 );
			}
		}
		return 1; // Formality
	}
