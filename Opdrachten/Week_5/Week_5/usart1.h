/*
 * PeakTech8k.h
 *
 * Created: 27/02/2014 11:59:27
 *  Author: Kraegon
 */ 


#ifndef PEAKTECH8K_H_
#define PEAKTECH8K_H_
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

void usart1_init( void )	;						// initialize USART0 - receive/transmit

void usart1_start( void ) ;						// UART0 receiver & transmitter enable

char uart1_receiveChar() ;						// UART0: receive ch

int uart1_sendChar( char ch ) ;					// UART0: send ch

int uart1_receiveString( char* string) ;
#endif /* PEAKTECH8K_H_ */