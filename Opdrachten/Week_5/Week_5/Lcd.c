/* Project name:     Demo2_4 : demo LCD display
 * Author: TI-Avans, WvdE
 * Revision History: 
     20101220: - initial release;
 * Description:
     This is a demo of writing text to the LCD2x16 display 
	 the diaplay is connected to PORTC (see: manual BIGAVR6)
 * Test configuration:
     MCU:             ATmega128
     Dev.Board:       BIGAVR6
     Oscillator:      External Clock 08.0000 MHz
     Ext. Modules:    -
     SW:              AVR-GCC
 * NOTES:
     - Turn ON the PORT LEDs at SW12.1 - SW12.8
*/

#include <avr/io.h>
#include <util/delay.h>
#include <string.h>

#define BIT(x)	(1 << (x))

unsigned char EMPTY_CGRAM[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

char regel1[]= "dit is een demo ";
char regel2[]= "van LCD-display ";
char blinkRegel[] = "Blink me";
char shiftRegel[] = "Shift me";
char header[] = "HEADER";
char lichtkrantRegel[] = "En ik ben de text.";


// lcdCommand: schrijf command byte (RS=0)
// eerst hoge nibble, dan de lage
// 
void lcdCommand ( unsigned char command )
{
	PORTC = command & 0xF0;		// hoge nibble
	PORTC = PORTC | 0x08;		// start (EN=1)
	wait (40);					// wait 1 ms
	PORTC = 0x00;				// stop
	
	PORTC = (command & 0x0F) << 4;	// lage nibble, shift lage naar hoge bits
	PORTC = PORTC | 0x08;		// start (EN =1)
	wait (40);					// wait 1 ms
	PORTC = 0x00;				// stop (EN=0 RS=0)	
}

// Schrijf data byte (RS=1)
// eerst hoge nibble, dan de lage
// 
void lcdWriteChar( unsigned char dat )
{
	PORTC = dat & 0xF0;			// hoge nibble
	PORTC = PORTC | 0x0C;		// data (RS=1), start (EN =1)
	wait (40);					// wait 1 ms
	PORTC = 0x04;				// stop
		
	PORTC = (dat & 0x0F) << 4;		// lage nibble
	PORTC = PORTC | 0x0C;		// data (RS=1), start (EN =1)
	wait (40);					// pulse 1 ms
	PORTC = 0x00;				// stop (EN=0 RS=0)
}

void lcdDefinePattern(unsigned char animation[]){
	lcdCommand(0x40);          // Manipulate CGRAM 01xx xxxx
	int i;
	for (i = 0; i < 8; ++i) //With this custom char
	{
		lcdWriteChar(animation[i]);
	}
}

// Initialisatie LCD
//
void lcdInit(void)
{
	DDRC=0xFC;					// PORT C is output, bar pins 0 & 1 which aren't connected.	
	wait(20);				// Give the screen some time to start
	lcdCommand( 0x02 );			// return home
	lcdCommand( 0x28 );			// mode: 4 bits interface data,
							// 2 lines,5x8 dots
	lcdCommand( 0x0C );			// display: on, cursor off, blinking off
	lcdCommand( 0x06 );			// entry mode: cursor to right, no shift
	lcdCommand( 0x01 );         // Clear screen
	lcdCommand( 0x80 );			// RAM adress: 0, first position, line 1
	
	lcdDefinePattern(EMPTY_CGRAM);
	wait(20);				// Give the screen some time to start
}

void lcdWriteSomeChar(char a){
	lcdCommand(0x80);
	lcdWriteChar(a);
}

//
// write upper line
void lcdWriteLine1 ( char text1[] )
{
	// eerst de eerste 8 karakters = regel 1
	lcdCommand(0x80);			// first position, line 1, adress $00
	for ( int i=0; i<strlen(text1); ++i )
	{
		lcdWriteChar( text1[i] );
	}
} 

void lcdWriteOnPosition( char text6[], int cursorPos ){
	lcdCommand(0x01);
	// eerst de eerste 8 karakters = regel 1
	lcdCommand(0x80 + cursorPos);			// first position, line 1, adress $00
	for ( int i=0; i<strlen(text6); ++i )
	{
		lcdWriteChar( text6[i] );
	}
}

//
// write second line
void lcdWriteLine2 ( char text2[] )
{
	// dan de tweede 8 karakters = regel 2
	lcdCommand(0x01);
	lcdCommand(0xC0);			// first position, line 2, adress $40
	for ( int i=0; i<strlen(text2); ++i )
	{
		lcdWriteChar( text2[i] );
	}
} 

void lcdBlinkText( char text3[])
{
	lcdCommand(0xC0);			//Go to pos 0
	for ( int i=0; i<strlen(text3); ++i )
	{
		lcdWriteChar( text3[i] );
	}
	
	wait(2500);
	lcdCommand(0x01);
	wait(2500);	
	lcdBlinkText(text3);	//Recursive call with no stop condition. 
}

void lcdTextShimmy( char text4[], int range ){
	lcdCommand(0xC0);			//Go to pos 0
	for ( int i=0; i<16; ++i )
	{
		lcdWriteChar( text4[i] );
	}
	for (int j = 0; j < range; ++j)
	{
		if(j >= (range/2))
			lcdShift(-1);
		else
			lcdShift(1);		
		wait(1000);
	}		
}

void lcdHeadedTextScroll(char header[], char text5[], int amplitude){
	if(amplitude > 16)
		amplitude = 0;
	lcdCommand(0x80); //Set cursor to top line plus one (Will be shifted by an amplitude later)
	for ( int i=0; i<16; ++i )
	{
		lcdWriteChar( header[i] );
	}
	lcdShift(-amplitude);
	lcdCommand(0xC0); //Set cursor to the bottom line
	for ( int i=0; i<16; ++i )
	{
		lcdWriteChar( text5[i] );
	}
	lcdShift(amplitude);
	wait(500);
	++amplitude;
	lcdHeadedTextScroll(header, text5, amplitude);	
}

//
// shift characters left or right
void lcdShift(int displacement)
{
	// size of the displacement
	int number = displacement<0 ? -displacement : displacement;
	for (int i=0; i<number; i++)
	{
		if (displacement <0)
			lcdCommand(0x18);
		else
			lcdCommand(0x1C);
	}
}

// main program - Tests animation
int main2_4 (void)
{
	wait(10);
	lcdCommand(0x01);
	lcdWriteLine1(regel1);
	return 0;
}// end program 

//Main program - Blink text (2_5a)
int main2_5() 
{
	//lcdBlinkText(blinkRegel); //Assignment A
	//lcdTextShimmy(shiftRegel, 20); //Assignment B
	lcdHeadedTextScroll(header,lichtkrantRegel,0);
}


//Following: Animation, a fun waste of time


//Frame by frame animation
void lcdAnimation(){
	//I got a lot of time to waste. And the code's ugly too.

	//I'm having fun, though.
	unsigned char animationA[8];
	animationA[0] = 0x00;  // 00000
	animationA[1] = 0x00;  // 00000
	animationA[2] = 0x00;  // 00000
	animationA[3] = 0x00;  // 00000
	animationA[4] = 0x00;  // 00000
	animationA[5] = 0x00;  // 00000
	animationA[6] = 0x10;  // 10000
	animationA[7] = 0x00;  // 00000

	unsigned char animationB[8];
	animationB[0] = 0x00;
	animationB[1] = 0x00;
	animationB[2] = 0x00;
	animationB[3] = 0x00;
	animationB[4] = 0x00;
	animationB[5] = 0x10;
	animationB[6] = 0x08;
	animationB[7] = 0x00;

	unsigned char animationC[8];
	animationC[0] = 0x00;
	animationC[1] = 0x10;
	animationC[2] = 0x10;
	animationC[3] = 0x10;
	animationC[4] = 0x10;
	animationC[5] = 0x10;
	animationC[6] = 0x18;
	animationC[7] = 0x10;

	unsigned char animationD[8];
	animationD[0] = 0x00;
	animationD[1] = 0x18;
	animationD[2] = 0x18;
	animationD[3] = 0x18;
	animationD[4] = 0x08;
	animationD[5] = 0x08;
	animationD[6] = 0x18;
	animationD[7] = 0x08;

	unsigned char animationE[8];
	animationE[0] = 0x00;
	animationE[1] = 0x0C;
	animationE[2] = 0x0C;
	animationE[3] = 0x0C;
	animationE[4] = 0x04;
	animationE[5] = 0x0C;
	animationE[6] = 0x16;
	animationE[7] = 0x04;

	unsigned char animationF[8];
	animationF[0] = 0x00;
	animationF[1] = 0x0C;
	animationF[2] = 0x0C;
	animationF[3] = 0x0C;
	animationF[4] = 0x04;
	animationF[5] = 0x04;
	animationF[6] = 0x0C;
	animationF[7] = 0x04;

	unsigned char animationG[8];
	animationG[0] = 0x00;
	animationG[1] = 0x0C;
	animationG[2] = 0x0C;
	animationG[3] = 0x0C;
	animationG[4] = 0x04;
	animationG[5] = 0x04;
	animationG[6] = 0x04;
	animationG[7] = 0x04;

	unsigned char animationH[8];
	animationH[0] = 0x00;
	animationH[1] = 0x0E;
	animationH[2] = 0x0C;
	animationH[3] = 0x0E;
	animationH[4] = 0x04;
	animationH[5] = 0x0E;
	animationH[6] = 0x04;
	animationH[7] = 0x04;

	unsigned char animationI[8]; //Reusable stick figure frame, in case the animation looks as stupid as I think.
	animationI[0] = 0x00; // 00000
	animationI[1] = 0x0E; // 01110
	animationI[2] = 0x0A; // 01010
	animationI[3] = 0x0E; // 01110
	animationI[4] = 0x04; // 00100
	animationI[5] = 0x0E; // 01110
	animationI[6] = 0x15; // 10101
	animationI[7] = 0x04; // 00100

	unsigned char animationJ[8];
	animationJ[0] = 0x00;
	animationJ[1] = 0x0E;
	animationJ[2] = 0x0A;
	animationJ[3] = 0x0E;
	animationJ[4] = 0x05;
	animationJ[5] = 0x0E;
	animationJ[6] = 0x14;
	animationJ[7] = 0x04;
	//Repeat I

	unsigned char animationK[8];
	animationK[0] = 0x00;
	animationK[1] = 0x0E;
	animationK[2] = 0x06;
	animationK[3] = 0x0E;
	animationK[4] = 0x04;
	animationK[5] = 0x0E;
	animationK[6] = 0x04;
	animationK[7] = 0x04;

	unsigned char animationL[8];
	animationL[0] = 0x00;
	animationL[1] = 0x06;
	animationL[2] = 0x06;
	animationL[3] = 0x06;
	animationL[4] = 0x04;
	animationL[5] = 0x04;
	animationL[6] = 0x04;
	animationL[7] = 0x04;

	unsigned char animationM[8];
	animationM[0] = 0x00;
	animationM[1] = 0x06;
	animationM[2] = 0x06;
	animationM[3] = 0x06;
	animationM[4] = 0x04;
	animationM[5] = 0x04;
	animationM[6] = 0x0C;
	animationM[7] = 0x04;

	unsigned char animationN[8];
	animationN[0] = 0x00;
	animationN[1] = 0x0C;
	animationN[2] = 0x0C;
	animationN[3] = 0x0C;
	animationN[4] = 0x08;
	animationN[5] = 0x18;
	animationN[6] = 0x0C;
	animationN[7] = 0x08;

	unsigned char animationO[8];
	animationO[0] = 0x00;
	animationO[1] = 0x18;
	animationO[2] = 0x18;
	animationO[3] = 0x18;
	animationO[4] = 0x10;
	animationO[5] = 0x18;
	animationO[6] = 0x14;
	animationO[7] = 0x10;

	unsigned char animationP[8];
	animationP[0] = 0x00;
	animationP[1] = 0x10;
	animationP[2] = 0x10;
	animationP[3] = 0x10;
	animationP[4] = 0x00;
	animationP[5] = 0x00;
	animationP[6] = 0x10;
	animationP[7] = 0x00;

	unsigned int animationTiming = 800;  //If I knew how to time using the clock, I'd use that. I'll be back.
	unsigned char waves = 2;			  //How many hearty hello's must mister LCD bellow.
	int i;                                //Keeping defines clean for the radio. Not neccesary beyond C99.
	printf("Start \n");
	lcdCommand(0x01);
	lcdCommand(0xC0);
	lcdWriteChar(0x00);
	lcdDefinePattern(animationA);
	wait(animationTiming);
	lcdDefinePattern(animationB);
	wait(animationTiming);
	lcdDefinePattern(animationC);
	wait(animationTiming);
	lcdDefinePattern(animationD);
	wait(animationTiming);
	lcdDefinePattern(animationE);
	wait(animationTiming);
	lcdDefinePattern(animationF);
	wait(animationTiming);
	lcdDefinePattern(animationG);
	wait(animationTiming);
	lcdDefinePattern(animationH);
	wait(animationTiming);
	for(i=0;i<waves;++i)
	{
		lcdDefinePattern(animationI);
		lcdCommand(0x84);
		lcdWriteChar('H');
		lcdWriteChar('e');
		lcdWriteChar('l');
		lcdWriteChar('l');
		lcdWriteChar('o');
		lcdWriteChar('!');
		wait(animationTiming*3);
		lcdDefinePattern(animationJ);
		wait(animationTiming *3);
		lcdCommand(0x01);
		lcdCommand(0xC0);
		lcdWriteChar(0x00);
		lcdDefinePattern(animationI);
		wait(animationTiming *3);
	}

	printf("Finalise \n");
	lcdDefinePattern(animationK);
	wait(animationTiming);
	lcdDefinePattern(animationL);
	wait(animationTiming);
	lcdDefinePattern(animationM);
	wait(animationTiming);
	lcdDefinePattern(animationN);
	wait(animationTiming);
	lcdDefinePattern(animationO);
	wait(animationTiming);
	lcdDefinePattern(animationP);
	wait(animationTiming);
	printf("End\n");
	lcdCommand(0x01);
	wait(animationTiming);	
}