/* Project name		: Demo5_1 : UART - send / receive characters with waiting
 * Author				: Avans-TI, JW, Julian G. West
 * Revision History	: 20110227: - initial release;
					  20140227: -
 * Description			: This program receive a character, and sends it back
 * Test configuration	: MCU:             ATmega128
     					  Dev.Board:       BIGAVR6
     					  Oscillator:      External Clock 08.0000 MHz
     					  Ext. Modules:    -
     					  SW:              AVR-GCC
 * NOTES				: Turn ON switch 12, PEO/PE1 tot RX/TX
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "Lcd.h"

#define BIT(x)			(1 << (x))
#define UART0_BAUD		2400						// Baud rate USART0
#define MYUBRR			8000000ul/16/UART0_BAUD - 1	// My USART Baud Rate Register
#define LF				'\n'

void usart0_init( void )					// initialize USART0 - receive/transmit
{
	int	ubrr = MYUBRR;
	UBRR0H = ubrr>>8;						// baudrate register, hoge byte
	UBRR0L = ubrr;							// baudrate register, lage byte
	UCSR0C = 0b00000110;					// asynchroon, 8 data - 1 stop - no parity
	UCSR0B = 0b00000000;					// receiver & transmitter disable
}

void usart0_start( void )					// receiver & transmitter enable
{
	UCSR0B = BIT(RXEN)|BIT(TXEN);		// set bit RXEN = Receiver enable 
	//   and TXEN = Transmitter enable
}

int uart0_sendChar( char ch )
{
	while (!(UCSR0A & BIT(UDRE0)));		// wait until UDRE0 is set: tx buffer is ready
	UDR0 = ch;								// send ch
	return 0;								// OK
}

char uart0_receiveChar( void )
{
	while (!(UCSR0A & BIT(RXC0)));		// if RX0 is set: unread data present in buffer
	char received = UDR0;
	if((received > 96) && (received < 123))
		received -= 32;
	return received;							// read ch
}

int uart0_receiveString( char* string)
{
	int 	stop = 0;						// boolean for stop value
	char	ch;

	while (!stop)							// while contunie
	{
		ch = uart0_receiveChar();			// read ch
		if ( ch == LF )					// stop at LF
		stop = 1;
		else
		*string++ = ch;				// else fill buffer
	}
	*string = '\0';						// string terminator
	
	return 0;
}
