/*
 * Week_5.c
 *
 * Created: 27/02/2014 11:49:42
 *  Author: Kraegon
 */ 

#include "usart0.h"
#include "SPI.h"
#include "Lcd.h"
#include <avr/io.h>
#include <util/delay.h>
// wait(): busy waiting for 'ms' millisecond - used library: util/delay.h
void wait( int ms )
{
	for (int tms=0; tms<ms; tms++)
	{
		_delay_ms( 1 );					// library function (max 30 ms at 8MHz)
	}
}

int main(void)
{
	lcdInit();
    main5_6();
}

int main5_6( void ){
	DDRB=0x0F;					// Set PB0 pin as output for display select
	spi_masterInit();              	// Initialize spi module
	displayDriverInit();            // Initialize display chip
	for (char i =1; i<=4; i++)
	{
		spi_writeWord(i,0);
	}
	wait(5000);
	int *num = malloc(sizeof(int));
	*num = 137;
	spi_displayNumber(num);
	free(num);
	return (1);
}

int main5_5( void ){
	DDRB=0x01;					// Set PB0 pin as output for display select
	spi_masterInit();              	// Initialize spi module
	displayDriverInit();            // Initialize display chip

	// clear display (all zero's)
	for (char i =1; i<=4; i++)
	{
		spi_writeWord(i,0);
	}
	wait(5000);
	// write 4-digit data
	for (char i =4; i>=1; i--)	//EDITED: Upped to 4
	{
		spi_writeWord(5-i,i);
		wait(2000);
	}
	return (1);
}


int main5_4( void ){
	//Nope
}

int main5_3( void ){
	char buffer[16];							// declare string buffer
	char buffer2[16];							// declare string buffer 2
	DDRB = 0xFF;								// set PORTB for output

	lcdInit();
	usart1_init();
	usart1_start();									// initialize LCD-display
	usart0_init();								// initialize USART0
	usart0_start();
	buffer[1] = 0;
	while (1)
	{
		wait(50);								// every 50 ms (busy waiting)
		PORTB ^= BIT(7);						// toggle bit 7 for testing
		//uart1_receiveString( buffer2 );
		//lcdWriteLine1(buffer2);
		buffer[0] = uart0_receiveChar();
		//uart0_receiveString( buffer );			// receive string from uart
		lcdWriteLine1(buffer);
	}

}

// Main program: USART0: send & receive
int main5_2( void )
{
	DDRB = 0xFF;							// set PORTB for output, for testing
	DDRA = 0xFF;							// set PORTA for output, for testing
	usart0_init();							// initialize USART0
	usart0_start();						// uart0: start send & receive
	char character;
	while (1)
	{
		wait(50);							// every 50 ms (busy waiting)
		PORTB ^= BIT(7);					// toggle bit 7 for testing
		
		character = uart0_receiveChar();	// read char
		lcdWriteSomeChar(character);
		PORTA = character;					// show read character, for testing

		uart0_sendChar(character);		// send back
		wait(2000);
	}
}