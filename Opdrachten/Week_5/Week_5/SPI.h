/*
 * SPI.h
 *
 * Created: 27/02/2014 15:21:43
 *  Author: Kraegon
 */ 


#ifndef SPI_H_
#define SPI_H_

void spi_masterInit(void);

void spi_write( unsigned char data );

char spi_writeRead( unsigned char data );

void spi_slaveSelect(unsigned char chipNumber);

void spi_slaveDeSelect(unsigned char chipNumber);

void displayDriverInit();

void displayOn();

void displayOff();

#endif /* SPI_H_ */