/*
 * Project name		: Demo5_7a : spi - 7 segments display
 * Author			: Avans-TI, WvdE, JW
 * Revision History	: 20110228: - initial release; 20120307: - this version
 * Description		: This program sends data to a 4-digit display with spi
 * Test configuration:
     	MCU:             ATmega128
     	Dev.Board:       BIGAVR6
     	Oscillator:      External Clock 08.0000 MHz
     	Ext. Modules:    Serial 7-seg display
     	SW:              AVR-GCC
 * NOTES			: Turn ON switch 15, PB1/PB2/PB3 to MISO/MOSI/SCK
*/

#include <avr/io.h>
#include <util/delay.h>

#define BIT(x)		( 1<<x )
#define DDR_SPI		DDRB		// spi Data direction register
#define PORT_SPI	PORTB		// spi Output register
#define SPI_SCK		1		// PB1: spi Pin System Clock
#define SPI_MOSI	2		// PB2: spi Pin MOSI
#define SPI_MISO	3		// PB3: spi Pin MISO
#define SPI_SS		0		// PB0: spi Pin Slave Select

void spi_masterInit(void)
{
	DDR_SPI = 0xff;					// All pins output: MOSI, SCK, SS, SS_display
	DDR_SPI &= ~BIT(SPI_MISO);			// 	except: MISO input
	PORT_SPI |= BIT(SPI_SS);			// SS_ADC == 1: deselect slave
	SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR1);	// or: SPCR = 0b11010010;
// Enable spi, MasterMode, Clock rate fck/64
//  	bitrate=125kHz, Mode = 0: CPOL=0, CPPH=0
}

void spi_writeWord ( unsigned char address, unsigned char data ){
	spi_slaveSelect(0); 		// Select display chip
	spi_write(address);  		// 	digit adress
	spi_write(data);			// 	digit value
	spi_slaveDeSelect(0);	// Deselect display chip
}

void spi_displayNumber( int *data){
	if((*data > 999)||(*data < -999)) //No numbers beyond 3 digits
		return;
	char *debug = malloc(16);
	int *counter = malloc(2*sizeof(int)); //Remember, 0 counts too.
	int *toWrite = malloc(2*sizeof(int));
	*counter = 2;
	if(*data<0){
		spi_writeWord(4,10);//Write '-', already declared in table.
		*data = *data * -1;		
	}
	while(*data>0){//repeat until data is a single digit
		*toWrite = (int) ((*data)/(int)(pow(10,*counter)+0.1));//Create single digit of the last digit of the number e.g. 321 -> 3, 21 -> 2 
		if(*toWrite >= 1) //Only if it's a valid number
			spi_writeWord(*counter+1,*toWrite); //Display on offset
		*data = *data % (int)(pow(10,*counter)+0.1); //Trim off the numbers first digit e.g. 321 -> 21 -> 1
		*counter = *counter - 1; //next digit
	}
	free(counter);
	free(toWrite);
}

// Write a byte from master to slave
void spi_write( unsigned char data )				
{
	SPDR = data;					// Load byte --> starts transmission
	while( !(SPSR & BIT(SPIF)) ); 		// Wait for transmission complete 
}

// Write a byte from master to slave and read a byte from slave - not used here
char spi_writeRead( unsigned char data )
{
	SPDR = data;					// Load byte --> starts transmission
	while( !(SPSR & BIT(SPIF)) ); 		// Wait for transmission complete 
	data = SPDR;					// New received data (eventually, MISO) in SPDR
	return data;					// Return received byte
}

// Select device on pinnumer PORTB
void spi_slaveSelect(unsigned char chipNumber)
{
	PORTB &= ~BIT(chipNumber);
}

// Deselect device on pinnumer PORTB
void spi_slaveDeSelect(unsigned char chipNumber)
{
	PORTB |= BIT(chipNumber);
}
// Initialize the driver chip (type MAX 7219)
void displayDriverInit() 
{
	spi_slaveSelect(0);			// Select display chip (MAX7219)
  	spi_write(0x09);      		// Register 09: Decode Mode
  	spi_write(0xFF);			// 	-> 1's = BCD mode for all digits
  	spi_slaveDeSelect(0);		// Deselect display chip

  	spi_slaveSelect(0);			// Select dispaly chip
  	spi_write(0x0A);      		// Register 0A: Intensity
  	spi_write(0x0F);    		//  -> Level 15 (in range [1..F]) EDITED: Maximum brightness
  	spi_slaveDeSelect(0);		// Deselect display chip

  	spi_slaveSelect(0);			// Select display chip
  	spi_write(0x0B);  			// Register 0B: Scan-limit
  	spi_write(0x03);   			// 	-> 3 = Display digits 0..3 EDITED: 4 displays
  	spi_slaveDeSelect(0);		// Deselect display chip

  	spi_slaveSelect(0);			// Select display chip
  	spi_write(0x0C); 			// Register 0B: Shutdown register
  	spi_write(0x01); 			// 	-> 1 = Normal operation
  	spi_slaveDeSelect(0);		// Deselect display chip
}

// Set display on ('normal operation')
void displayOn() 
{
  	spi_slaveSelect(0);			// Select display chip
  	spi_write(0x0C); 			// Register 0B: Shutdown register
  	spi_write(0x01); 			// 	-> 1 = Normal operation
  	spi_slaveDeSelect(0);		// Deselect display chip
}

// Set display off ('shut down')
void displayOff() 
{
  	spi_slaveSelect(0);			// Select display chip
  	spi_write(0x0C); 			// Register 0B: Shutdown register
  	spi_write(0x00); 			// 	-> 1 = Normal operation
  	spi_slaveDeSelect(0);		// Deselect display chip
}
