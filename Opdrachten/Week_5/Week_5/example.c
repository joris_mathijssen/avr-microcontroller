/*
 * Project name		: Demo5_7a : spi - 7 segments display
 * Author			: Avans-TI, WvdE, JW
 * Revision History	: 20110228: - initial release; 20120307: - this version
 * Description		: This program sends data to a 4-digit display with spi
 * Test configuration:
     	MCU:             ATmega128
     	Dev.Board:       BIGAVR6
     	Oscillator:      External Clock 08.0000 MHz
     	Ext. Modules:    Serial 7-seg display
     	SW:              AVR-GCC
 * NOTES			: Turn ON switch 15, PB1/PB2/PB3 to MISO/MOSI/SCK
*/

#include <avr/io.h>
#include <util/delay.h>

#define BIT(x)		( 1<<x )
#define DDR_SPI		DDRB		// spi Data direction register
#define PORT_SPI	PORTB		// spi Output register
#define SPI_SCK		1		// PB1: spi Pin System Clock
#define SPI_MOSI	2		// PB2: spi Pin MOSI
#define SPI_MISO	3		// PB3: spi Pin MISO
#define SPI_SS		0		// PB0: spi Pin Slave Select

// wait2(): busy wait2ing for 'ms' millisecond  - used library: util/delay.h
void wait2(int ms)
{
	for (int i=0; i<ms; i++)
		_delay_ms(1);
}

void spi_masterInit2(void)
{
	DDR_SPI = 0xff;					// All pins output: MOSI, SCK, SS, SS_display
	DDR_SPI &= ~BIT(SPI_MISO);			// 	except: MISO input
	PORT_SPI |= BIT(SPI_SS);			// SS_ADC == 1: deselect slave
	SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR1);	// or: SPCR = 0b11010010;
// Enable spi, MasterMode, Clock rate fck/64
//  	bitrate=125kHz, Mode = 0: CPOL=0, CPPH=0
}

// Write a byte from master to slave
void spi_write2( unsigned char data )				
{
	SPDR = data;					// Load byte --> starts transmission
	while( !(SPSR & BIT(SPIF)) ); 		// wait2 for transmission complete 
}

// Write a byte from master to slave and read a byte from slave - not used here
char spi_write2Read( unsigned char data )
{
	SPDR = data;					// Load byte --> starts transmission
	while( !(SPSR & BIT(SPIF)) ); 		// wait2 for transmission complete 
	data = SPDR;					// New received data (eventually, MISO) in SPDR
	return data;					// Return received byte
}

// Select device on pinnumer PORTB
void spi_slaveSelect2(unsigned char chipNumber)
{
	PORTB &= ~BIT(chipNumber);
}

// Deselect device on pinnumer PORTB
void spi_slaveDeSelect2(unsigned char chipNumber)
{
	PORTB |= BIT(chipNumber);
}
// Initialize the driver chip (type MAX 7219)
void displayDriverInit2() 
{
	spi_slaveSelect2(0);			// Select display chip (MAX7219)
  	spi_write2(0x09);      		// Register 09: Decode Mode
  	spi_write2(0xFF);			// 	-> 1's = BCD mode for all digits
  	spi_slaveDeSelect2(0);		// Deselect display chip

  	spi_slaveSelect2(0);			// Select dispaly chip
  	spi_write2(0x0A);      		// Register 0A: Intensity
  	spi_write2(0x04);    			//  -> Level 4 (in range [1..F])
  	spi_slaveDeSelect2(0);		// Deselect display chip

  	spi_slaveSelect2(0);			// Select display chip
  	spi_write2(0x0B);  			// Register 0B: Scan-limit
  	spi_write2(0x01);   			// 	-> 1 = Display digits 0..1
  	spi_slaveDeSelect2(0);		// Deselect display chip

  	spi_slaveSelect2(0);			// Select display chip
  	spi_write2(0x0C); 			// Register 0B: Shutdown register
  	spi_write2(0x01); 			// 	-> 1 = Normal operation
  	spi_slaveDeSelect2(0);		// Deselect display chip
}

// Set display on ('normal operation')
void displayOn2() 
{
  	spi_slaveSelect2(0);			// Select display chip
  	spi_write2(0x0C); 			// Register 0B: Shutdown register
  	spi_write2(0x01); 			// 	-> 1 = Normal operation
  	spi_slaveDeSelect2(0);		// Deselect display chip
}

// Set display off ('shut down')
void displayOff2() 
{
  	spi_slaveSelect2(0);			// Select display chip
  	spi_write2(0x0C); 			// Register 0B: Shutdown register
  	spi_write2(0x00); 			// 	-> 1 = Normal operation
  	spi_slaveDeSelect2(0);		// Deselect display chip
}

int exexmain()
{
	DDRB=0x01;					// Set PB0 pin as output for display select
	spi_masterInit2();              	// Initialize spi module
	displayDriverInit2();            // Initialize display chip

 	// clear display (all zero's)
	for (char i =1; i<=2; i++)
	{
      	spi_slaveSelect2(0); 		// Select display chip
      	spi_write2(i);  			// 	digit adress: (digit place)
      	spi_write2(0);			// 	digit value: 0 
  	  	spi_slaveDeSelect2(0);	// Deselect display chip
	}    
	wait2(1000);

	// write 4-digit data  
 	for (char i =1; i<=2; i++)
  	{
		spi_slaveSelect2(0);       // Select display chip
		spi_write2(i);         	// 	digit adress: (digit place)
		spi_write2(i);  			// 	digit value: i (= digit place)
		spi_slaveDeSelect2(0); 		// Deselect display chip
	
		wait2(1000);
  	}
	wait2(1000);
  	return (1);
}