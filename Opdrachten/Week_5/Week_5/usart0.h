/*
 * PeakTech8k.h
 *
 * Created: 27/02/2014 11:59:27
 *  Author: Kraegon
 */ 


#ifndef PEAKTECH8K_H_
#define PEAKTECH8K_H_
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define BIT(x)			(1 << (x))
#define UART0_BAUD		9600						// Baud rate USART0
#define MYUBRR			F_CPU/16/UART0_BAUD - 1	// My USART Baud Rate Register

extern int main5_2(void);

extern int main5_3(void);

int uart0_receiveString( char* string) ;

void usart0_init( void )	;						// initialize USART0 - receive/transmit

void usart0_start( void ) ;						// UART0 receiver & transmitter enable

char uart0_receiveChar() ;						// UART0: receive ch

int uart0_sendChar( char ch ) ;					// UART0: send ch

#endif /* PEAKTECH8K_H_ */
