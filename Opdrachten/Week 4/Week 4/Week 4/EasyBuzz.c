/*
 * Project name: 	 	EasyBuzz:  	PWM with timer 1 Fast PWM mode 
									at PORTB.5 = OCR1A generates melodic tones
 * Author: 				Avans-TI, Julian G. West -- Edit on original by JW
 * Revision History: 	20101230: - initial release;
						20140220: - Melody revision;
 * Description:			This program give an array of tones;
 * Test configuration:
    			 	MCU:             ATmega128
     			Dev.Board:       BIGAVR6
     			Oscillator:      External Clock 08.0000 MHz
     			Ext. Modules:    -
     			SW:              AVR-GCC
 * NOTES:		Turn ON the PORT LEDs at SW12.1 - SW12.8
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include "EasyBuzz.h"

#define BIT(x)		(1 << (x))
#define ASINTERVAL(f) ((1/(f))*100)			//Zet frequency om in interval

int durations[6] = {20000-2000, 10000-1000, 5000-500, 2500-250, 1250-125, 625-62};

void playLowGSharp(int duration);
void playLowA(int duration);
void playLowASharp(int duration);
void playLowB(int duration);
void playC(int duration);
void playCSharp(int duration);
void playD(int duration);
void playDSharp(int duration);
void playE(int duration);
void playF(int duration);
void playFSharp(int duration);
void playG(int duration);
void playGSharp(int duration);
void playA(int duration);
void playASharp(int duration);
void playB(int duration);
void playHighC(int duration);
void playHighCSharp(int duration);
void playHighD(int duration);
void playHighDSharp(int duration);
void playHighE(int duration);
void playHighF(int duration);
void playHighFSharp(int duration);
void playHighG(int duration);
void playHighGSharp(int duration);
void playHighA(int duration);
void playHighASharp(int duration);
void playHighB(int duration);
void playHigherC(int duration);
void playHigherCSharp(int duration);

void DarkWorld();
void Frog();
void MetalMan();
void EightMelodies();


// Initialize timer 1: fast PWM at pin PORTB.5    (time_interval = INTERVAL * 1 us)
void timer1Init( void )
{
	TCCR1A = 0b10000010;			// timer, compare output at OC1A=PB5
	TCCR1B = 0b00011010;			// fast PWM, TOP = ICR1, prescaler=8 (1 MHz), RUN
}


// Main program: PWM with T1
int main4_1( void )
{
	DDRB = 0xFF;					// set PORTB and PORTA for compare output 
	DDRA = 0xFF;
	timer1Init();					// Initialize timer 1

	while (1)
	{
		//Select a song:
		//DarkWorld() -- Dark World -- Legend of Zelda; A link to the Past
		//Frog() -- Frog's Theme -- Chrono Trigger
		//MetalMan() -- Metal Man stage -- Megaman 2
		//EightMelodies() -- Eight melodies -- Earthbound(Mother 2)
		MetalMan();
	}
}

void EightMelodies(){
	playD(durations[2]);
	playE(durations[2]);
	playFSharp(durations[2]);
	playA(durations[2]);
	playE(durations[0]);
	playHighD(durations[2]);
	playHighCSharp(durations[2]);
	playHighB(durations[2]);
	playHighFSharp(durations[2]);
	playA(durations[0]);
	playB(durations[2]);
	playHighCSharp(durations[2]);
	playHighD(durations[2]);
	playA(durations[1]);
	playD(durations[1] + durations[2]);
	playG(durations[2]);
	playFSharp(durations[2]);
	playD(durations[2]);
	playLowA(durations[2] + durations[0]);
	playLowB(durations[1]);
	playCSharp(durations[1]);
	playD(durations[1]);
	playG(durations[1]);
	playFSharp(durations[2]);
	playG(durations[2]);
	playA(durations[2]);
	playFSharp(durations[2]);
	playE(durations[2]);
	playFSharp(durations[2]);
	playG(durations[2]);
	playE(durations[2]);
	playLowB(durations[2]);
	playG(durations[2]);
	playFSharp(durations[2]);
	playD(durations[2]);
	playE(durations[1]);
	playCSharp(durations[1]);
	playD(durations[1]);
	playLowA(durations[2]);
	playE(durations[2]);
	playD(durations[0]);
	wait(durations[0]);
}

void DarkWorld(){
	 playHighC(durations[3]);
	 playHighG(durations[1]);
	 playHighC(durations[3]);
	 playHighG(durations[3]);
	 playHigherC(durations[3]);
	 playHighA(durations[4]);
	 playHighASharp(durations[4]);
	 playHighA(15250);
	 playHighF(durations[4]);
	 playHighG(durations[2]);
	 playHighC(durations[2]+durations[2]+durations[3]);
	 playHighF(durations[3]);
	 playHighD(durations[4]);
	 playHighDSharp(durations[4]);
	 playHighD(durations[3] + durations[1] + durations[3] + durations[4]);
	 playASharp(durations[4]);
	 playHighC(durations[1] + durations[2]);
	 playHighC(durations[4]);
	 playHighG(durations[4]);
	 playHighD(durations[1] + durations[2]);

	 playHighC(durations[3]);
	 playHighG(durations[1]);
	 playHighC(durations[3]);
	 playHighG(durations[3]);
	 playHigherC(durations[3]);
	 playHighA(durations[4]);
	 playHighASharp(durations[4]);
	 playHighA(15250);
	 playHighF(durations[4]);
	 playHighG(durations[2]);
	 playHighC(durations[2]+durations[2]+durations[3]);
	 playHighF(durations[3]);
	 playHighD(durations[4]);
	 playHighDSharp(durations[4]);
	 playHighD(durations[3] + durations[1] + durations[3] + durations[4]);
	 playASharp(durations[4]);
	 playHighC(durations[1] + durations[2]);
	 playHighF(durations[4]);
	 playHighDSharp(durations[4]);
	 playHighD(durations[4]);
	 playHighC(durations[4]);
	 playHighD(durations[1] + durations[2]);

	 playHighG(durations[4]);
	 playHighA(durations[4]);
	 playHighASharp(durations[1] + durations[2]);
	 playHighG(durations[3]);
	 playHighASharp(durations[3]);
	 playHighA(durations[4]);
	 playHighG(durations[4]);
	 playHighF(durations[3]+durations[1]+durations[3]);
	 playHighDSharp(durations[4]);
	 playHighF(durations[4]);
	 playHighG(durations[1]+durations[2]);
	 playHighC(durations[3]);
	 playHighG(durations[3]);
	 playHighF(durations[4]);
	 playHighDSharp(durations[4]);
	 playHighD(durations[3]+durations[1]);
	 playHighG(durations[4]);
	 playHighA(durations[4]);
	 playHighASharp(durations[1]+durations[2]);
	 playHighG(durations[3]);
	 playHighASharp(durations[3]);
	 playHighA(durations[4]);
	 playHighASharp(durations[4]);
	 playHigherC(durations[3]+durations[1]+durations[2]);
	 playHighASharp(durations[1]+durations[2]);
	 playHighA(durations[3]);
	 playHighASharp(durations[3]);
	 playHighA(durations[4]);
	 playHighGSharp(durations[4]);
	 playHighA(durations[3]+durations[1]+durations[2]);
	 playHighG(durations[0]);
	 wait(durations[0]);
	 playHighC(durations[4]);
	 playG(durations[4]);
	 playF(durations[4]);
	 playG(durations[4]+durations[1]);
	 playHighC(durations[2]);
	 playASharp(durations[4]);
	 playG(durations[4]);
	 playF(durations[4]);
	 playG(durations[4]+durations[1]+durations[2]);
	 playASharp(durations[4]);
	 playF(durations[4]);
	 playDSharp(durations[4]);
	 playF(durations[4]+durations[1]);
	 playASharp(durations[2]);
	 playA(durations[4]);
	 playDSharp(durations[4]);
	 playD(durations[4]);
	 playDSharp(durations[4]+durations[1]+durations[2]);
	 playG(durations[4]);
	 playD(durations[4]);
	 playCSharp(durations[4]);
	 playD(durations[4]+durations[1]);
	 playG(durations[4]);
	 playHighC(durations[4]);
	 playG(durations[4]);
	 playF(durations[4]);
	 playG(durations[4]+durations[1]);
	 playHighC(durations[2]);
	 playHighD(durations[0]);
	 wait(durations[2]);
 }
 
void MetalMan(){
	for(int i = 0; i < 2; ++i){
		playLowASharp(durations[1] + durations[2]);
		playLowASharp(durations[4]);
		playF(durations[3]);
		playDSharp(durations[4]);
		wait(durations[4]);
		playCSharp(durations[3] + durations[4]);
		playDSharp(durations[3]);
		playCSharp(durations[4]);
		playLowASharp(durations[4] + durations[2]);
		playLowGSharp(durations[4]);
		playLowGSharp(durations[3]);
		playLowASharp(durations[4] + durations[2]);
		wait(durations[3]);
		playC(durations[3]);
		playCSharp(durations[3] + durations[4]);
		playLowASharp(durations[4] + durations[2]);
		playHighCSharp(durations[2]);
		playHighDSharp(durations[2]);
		playHighCSharp(durations[3]);
		playHighCSharp(durations[3]);
		playHighDSharp(durations[2]);
	}
	for(int r = 0; r < 2; ++r){
		playASharp(durations[4]);
		playASharp(durations[4]);
		playGSharp(durations[3]);
		playASharp(durations[3]);
		playGSharp(durations[3]);
		playASharp(durations[2]);
		playF(durations[3]);
		playGSharp(durations[3]);
		wait(durations[3]);
		playASharp(durations[3] + durations[1]);
		playASharp(durations[4]);
		playHighF(durations[3]+ durations[4]);
		playHighDSharp(durations[2]);
		wait(durations[3]);
		playHighCSharp(durations[3]);
		playHighC(durations[3]+durations[4]);
		playHighCSharp(durations[4]+durations[3]);
		playHighC(durations[3]);
		playHighCSharp(durations[2]);
		playHighC(durations[3]+durations[4]);
		playHighCSharp(durations[4]);
		wait(durations[4]);
		playHighCSharp(durations[3]+durations[4]);
		playHighC(durations[3]);
		playHighC(durations[3]);

		playASharp(durations[4]);
		playASharp(durations[4]);
		playGSharp(durations[3]);
		playASharp(durations[3]);
		playGSharp(durations[3]);
		playASharp(durations[2]);
		playF(durations[3]);
		playGSharp(durations[3]);
		wait(durations[3]);
		playASharp(durations[3] + durations[1]);
		playASharp(durations[4]);
		playHighF(durations[3]+ durations[4]);
		playHighDSharp(durations[2]);
		wait(durations[3]);
		playHighCSharp(durations[3]);
		playHighC(durations[3]+durations[4]);
		playHighCSharp(durations[4]+durations[3]);
		playHighC(durations[3]);
		playHighCSharp(durations[3]);
		playHighCSharp(durations[3]);
		playHighC(durations[3]+durations[4]);
		playHighCSharp(durations[4]);
		wait(durations[2]);
		playASharp(durations[4]);
		playHighF(durations[3]+ durations[4]);
		playHighDSharp(durations[3]);
		playHighDSharp(durations[3]);
		playHighCSharp(durations[3]);
		playHighCSharp(durations[3]);
		playHighC(durations[3]);
		playHighC(durations[3]);
		playHighCSharp(durations[4]);
		playHighDSharp(durations[3]+durations[4]);
		playASharp(durations[3]);
		playASharp(durations[3]);
		playASharp(durations[4]);
		playGSharp(durations[3]+durations[4]);
		playASharp(durations[2]);
		playASharp(durations[4]);
		playHighF(durations[3]+durations[4]);
		playHighDSharp(durations[3]);
		playHighDSharp(durations[3]);
		playHighCSharp(durations[3]);
		playHighCSharp(durations[3]);
		playHighC(durations[3]);
		playHighC(durations[3]);
		playHighCSharp(durations[4]);
		playHighDSharp(durations[3]);
		playHighF(durations[4]);
		wait(durations[4]);
		playHighF(durations[3]+durations[4]);
		playHighF(durations[4]);
		playHighDSharp(durations[4]);
		playHighCSharp(durations[4]);
		playHighDSharp(durations[4]);
		playHighCSharp(durations[4]);
		playHighC(durations[4]);
		playHighCSharp(durations[4]);
		playHighC(durations[4]);
		playASharp(durations[4]);
		playHighC(durations[4]);
		playASharp(durations[4]);
		playGSharp(durations[4]);

		playHighDSharp(durations[3]);
		playHighDSharp(durations[3]);
		playHighCSharp(durations[3]);
		playHighCSharp(durations[3]);
		playHighC(durations[3]);
		playHighC(durations[3]);
		playHighCSharp(durations[4]);
		playHighDSharp(durations[3]+durations[4]);
		playASharp(durations[3]);
		playASharp(durations[3]);
		playASharp(durations[4]);
		playGSharp(durations[3]+durations[4]);
		playASharp(durations[2]);
		playASharp(durations[4]);
		playHighF(durations[3]+durations[4]);
		playHighDSharp(durations[3]);
		playHighDSharp(durations[3]);
		playHighCSharp(durations[3]);
		playHighCSharp(durations[3]);
		playHighC(durations[3]);
		playHighC(durations[3]);
		playHighCSharp(durations[4]);
		playHighDSharp(durations[3]);
		playHighF(durations[4]);
		wait(durations[4]);
		playHighF(durations[4]);
		playHighF(durations[3]);
		playHighF(durations[3]);
		playHighF(durations[3]);
		playHighF(durations[3]+durations[4]);
		playF(durations[4]);
		playF(durations[4]);
		playGSharp(durations[3] + durations[4]);
	}
	playASharp(durations[1]);
	wait(durations[1]);
}


void Frog(){
	playGSharp(durations[3] - durations[5] );
	playFSharp(durations[3] - durations[5]);
	playGSharp(durations[3] - durations[5]);
	playDSharp(durations[2]);
	playGSharp(durations[2]);
	playASharp(durations[3]);
	playB(durations[3]);
	playASharp(durations[3] - durations[5]);
	playGSharp(durations[3] - durations[5]);
	playASharp(durations[3] - durations[5]);
	playFSharp(durations[1]);
	playHighCSharp(durations[3] - durations[5]);
	playB(durations[3] - durations[5]);
	playASharp(durations[3] - durations[5]);
	playGSharp(durations[3] - durations[5] );
	playFSharp(durations[3] - durations[5]);
	playGSharp(durations[3] - durations[5]);
	playDSharp(durations[2]);
	playGSharp(durations[2]);
	playASharp(durations[3]);
	playB(durations[3]);
	playASharp(durations[3] - durations[5] );
	playB(durations[3] - durations[5]);
	playHighCSharp(durations[3] - durations[5]);
	playASharp(durations[1]);
	wait(durations[2]);
	playGSharp(durations[3] - durations[5] );
	playFSharp(durations[3] - durations[5]);
	playGSharp(durations[3] - durations[5]);
	playDSharp(durations[2]);
	playGSharp(durations[2]);
	playASharp(durations[3]);
	playB(durations[3]);
	playASharp(durations[3] - durations[5]);
	playGSharp(durations[3] - durations[5]);
	playASharp(durations[3] - durations[5]);
	playFSharp(durations[1]);
	playHighCSharp(durations[3] - durations[5]);
	playB(durations[3] - durations[5]);
	playASharp(durations[3] - durations[5]);
	playGSharp(durations[3] - durations[5] );
	playFSharp(durations[3] - durations[5]);
	playGSharp(durations[3] - durations[5]);
	playASharp(durations[2]);
	playB(durations[2]);
	playASharp(durations[2]);
	playGSharp(durations[3] - durations[5] );
	playFSharp(durations[3] - durations[5]);
	playGSharp(durations[3] - durations[5]);
	playGSharp(durations[3] - durations[5] );
	playFSharp(durations[3] - durations[5]);
	playGSharp(durations[3] - durations[5]);
	playGSharp(durations[2]);
	wait(durations[2]);

	playHighDSharp(durations[2] - durations[5]);
	playHighDSharp(durations[3] - durations[5]);
	playHighDSharp(durations[3]);
	playB(durations[3] - durations[5]);
	playHighCSharp(durations[3] - durations[5]);
	playHighDSharp(durations[2] - durations[5]);
	playHighGSharp(durations[2]);
	playHighFSharp(durations[1]);
	playHighDSharp(durations[2] + durations[2] - durations[5]);
	playB(durations[3] - durations[5]);
	playGSharp(durations[0]);

	playGSharp(durations[3] - durations[5]);
	playASharp(durations[3] - durations[5]);
	playB(durations[3] - durations[5]);
	playASharp(durations[3] - durations[5]);
	playB(durations[3] - durations[5]);
	playHighCSharp(durations[3] - durations[5]);
	playB(durations[3] - durations[5]);
	playHighCSharp(durations[3] - durations[5]);
	playHighDSharp(durations[3] - durations[5]);
	playHighCSharp(durations[3] - durations[5]);
	playHighDSharp(durations[3] - durations[5]);
	playHighE(durations[3] - durations[5]);
	playHighDSharp(durations[2]);
	playHighDSharp(durations[3] - durations[5]);
	playB(durations[3] - durations[5]);
	playHighCSharp(durations[3] - durations[5]);
	playHighDSharp(durations[2]);
	playHighGSharp(durations[2]);
	playHighASharp(durations[2] + durations[3] - durations[5]);
	playHighB(durations[3] - durations[5]);
	playHighASharp(durations[3] - durations[5]);
	playHighGSharp(durations[2]);
	playHighFSharp(durations[2]);
	playHighGSharp(durations[0]);
	playHighGSharp(durations[1]);
	playHighASharp(durations[1]);

	playHighB(durations[2]);
	playHighB(durations[3] - durations[5]);
	playHighASharp(durations[3] - durations[5]);
	playHighGSharp(durations[3] - durations[5]);
	playHighFSharp(durations[2]);
	playHighDSharp(durations[2]);
	playHighGSharp(durations[2] - durations[5]);
	playHighGSharp(durations[3] - durations[5]);
	playHighGSharp(durations[3] - durations[5]);
	playHighFSharp(durations[3] - durations[5]);
	playHighE(durations[3] - durations[5]);
	playHighDSharp(durations[1]);
	playHighB(durations[2] - durations[5]);
	playHighB(durations[3] - durations[5]);
	playHighB(durations[3] - durations[5]);
	playHighASharp(durations[3]-durations[5]);
	playHighB(durations[3] - durations[5]);
	playHigherCSharp(durations[2]);
	playHighASharp(durations[2]);
	playHighGSharp(durations[1]);
	playHighASharp(durations[1]);

	playHighB(durations[2]);
	playHighB(durations[3] - durations[5]);
	playHighASharp(durations[3] - durations[5]);
	playHighGSharp(durations[3] - durations[5]);
	playHighFSharp(durations[2]);
	playHighDSharp(durations[2]);
	playHighGSharp(durations[2] - durations[5]);
	playHighGSharp(durations[3] - durations[5]);
	playHighGSharp(durations[3] - durations[5]);
	playHighFSharp(durations[3] - durations[5]);
	playHighE(durations[3] - durations[5]);
	playHighDSharp(durations[1]);
	playHighB(durations[2] - durations[5]);
	playHighB(durations[3] - durations[5]);
	playHighB(durations[3] - durations[5]);
	playHighASharp(durations[3]-durations[5]);
	playHighB(durations[3] - durations[5]);
	playHigherCSharp(durations[2]);
	playHighGSharp(durations[3] - durations[5]);
	playHighFSharp(durations[3] - durations[5]);
	playHighGSharp(durations[3] - durations[5]);
	playHighGSharp(durations[3] - durations[5]);
	playHighFSharp(durations[3] - durations[5]);
	playHighGSharp(durations[3] - durations[5]);
	playHighGSharp(durations[3] - durations[5]);
}



//Tone enum

  void playLowGSharp(int duration){
		DDRB = 0xFF;							//Allow output
		double freq = 0.052;
		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
		wait(duration);
		DDRB = 0x00;							//Stop output
  }
  void playLowA(int duration){
	DDRB = 0xFF;
	double freq = 0.055;
	ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	wait(duration);
	DDRB = 0x00;	
  }
  void playLowASharp(int duration){
			DDRB = 0xFF;
	  		double freq = 0.058;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
  void playLowB(int duration){
			DDRB = 0xFF;
	  		double freq = 0.061;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);	  
			  DDRB = 0x00;	
  }
  void playC(int duration){
	  		DDRB = 0xFF;
			  double freq = 0.065;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
  void playCSharp(int duration){
			DDRB = 0xFF;
	  		double freq = 0.069;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playD(int duration){
			DDRB = 0xFF;
	  		double freq = 0.073;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playDSharp(int duration){
			DDRB = 0xFF;
	  		double freq = 0.077;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playE(int duration){
			DDRB = 0xFF;
	  		double freq = 0.082;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playF(int duration){
			DDRB = 0xFF;
	  		double freq = 0.087;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playFSharp(int duration){
			DDRB = 0xFF;
	  		double freq = 0.092;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playG(int duration){
			DDRB = 0xFF;
	  		double freq = 0.098;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playGSharp(int duration){
	  		DDRB = 0xFF;
			  double freq = 0.103;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playA(int duration){
			DDRB = 0xFF;
	  		double freq = 0.110;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playASharp(int duration){
			DDRB = 0xFF;
	  		double freq = 0.116;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playB(int duration){
			DDRB = 0xFF;
	  		double freq = 0.123;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHighC(int duration){
			DDRB = 0xFF;
	  		double freq = 0.130;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHighCSharp(int duration){
			DDRB = 0xFF;
	  		double freq = 0.138;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHighD(int duration){
			DDRB = 0xFF;
	  		double freq = 0.146;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHighDSharp(int duration){
			DDRB = 0xFF;
	  		double freq = 0.155;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHighE(int duration){
			DDRB = 0xFF;
	  		double freq = 0.164;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHighF(int duration){
			DDRB = 0xFF;
	  		double freq = 0.174;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHighFSharp(int duration){
			DDRB = 0xFF;
	  		double freq = 0.185;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHighG(int duration){
			DDRB = 0xFF;
	  		double freq = 0.196;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHighGSharp(int duration){
			DDRB = 0xFF;
	  		double freq = 0.207;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHighA(int duration){
			DDRB = 0xFF;
	  		double freq = 0.220;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHighASharp(int duration){
			DDRB = 0xFF;
	  		double freq = 0.233;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHighB(int duration){
			DDRB = 0xFF;
	  		double freq = 0.247;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHigherC(int duration){
	  		DDRB = 0xFF;
			  double freq = 0.261;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }
   void playHigherCSharp(int duration){
			DDRB = 0xFF;	
	  		double freq = 0.277;
	  		ICR1 = ASINTERVAL(freq);				// TOP value for counting = ICR1 = INTERVAL (*us)
	  		OCR1A = ASINTERVAL(freq)/2;				// compare value in between
	  		wait(duration);
			  DDRB = 0x00;	
  }