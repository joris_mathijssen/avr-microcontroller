/*
 * Week_4.c
 *
 * Created: 20/02/2014 10:59:39
 *  Author: Kraegon
 */ 


#include <avr/io.h>
#include <util/delay.h>
#include "EasyBuzz.h"

void wait (int ms);


// wait(): busy waiting for 'ms' millisecond
// Used library: util/delay.h
void wait( int ms )
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );			// library function (max 30 ms at 8MHz)
	}
}
int main(void)
{
    while(1)
    {		
        main4_2();
    }
}