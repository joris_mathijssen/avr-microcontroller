/*
 * Project name: 	RGBLED : 	PWM with timer 1 Fast PWM mode at PORTB.5, PB.6 en PB.7
	 				parallel, 3x, for RGB LED
 * Author: 			Avans-TI, Julian G. West Edited from: JW
 * Revision History:	20101230: - initial release;
						20140220: - Who honestly reads this? Seriously. Why do I try.
 * Description:		This program controls a RGB-LED with PWM of timer1
 * Test configuration:
     				MCU:             ATmega128
     				Dev.Board:       BIGAVR6
     				Oscillator:      External Clock 08.0000 MHz
     				Ext. Modules:    -
     				SW:              AVR-GCC
 * NOTES:			- Turn ON the PORT LEDs at SW12.1 - SW12.8
*/

#include <avr/io.h>
#include <avr/interrupt.h>

#define BIT(x)			(1 << (x))

// Initialize timer 1: fast PWM at pin PORTB.6 (hundredth ms)
void timer2Init( void )
{
	OCR1A = 0;						// default, off  (RED)
	OCR1B = 0;						// default, off	(GREEN)
	OCR1C = 0;						// default, off	(BLUE)
	TCCR1A = 0b10101001;				// compare output OC1A,OC1B,OC1C
	TCCR1B = 0b00001011;				// fast PWM 8 bit, prescaler=64, RUN
}

// Set pulse width for RED on pin PB5, 0=off, 255=max
void setRed( unsigned char redValue )
{
	OCR1A = redValue;
}

void setBlue( unsigned char blueValue )
{
	OCR1B = blueValue;
}
void setGreen( unsigned char greenValue )
{
	OCR1C = greenValue;
}

// Main program: Counting on T1
int main4_2( void )
{
	DDRB = 0xFF;						// set PORTB for compare output 
	timer2Init();					// initialize PWM on timer1
	wait(500);
	while (1)
	{
		// change intensity color in steps of 100 ms - fading
		int deltaBlue = 1;
		int deltaRed = 1;
		int deltaGreen = 1;
		for (int blue = 0; blue<=255; blue+=deltaBlue){
			setBlue( blue );				
			for(int green = 0; green<=255;green+=deltaGreen){
				setGreen(green);
				for(int red = 0; red<=255;red+=deltaRed){
					wait(50);
					setRed(red);									
					deltaRed += 2;									
				}
				deltaRed = 0;
				deltaGreen += 2;		
			}
			deltaGreen = 0;
			deltaBlue += 2;	
		}
		deltaBlue = 0;
	}
}
