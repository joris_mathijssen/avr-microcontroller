/*
 * _7segDisplay.c
 * Based upon example 2.2 of week 2
 *
 * Created: 12-2-2014 10:32:27
 *  Author: Kraegon
 */ 
#include "7segDisplay.h"
#include <avr/io.h>
#include <util/delay.h>

const unsigned char numbers [16] =
{
	//	  Pgfedcba
	0b00111111,		// 0 
	0b00000110,		// 1
	0b01011011,		// 2
	0b01001111,		// 3
	0b01100110,		// 4
	0b01101101,		// 5
	0b01111101,		// 6
	0b00000111,		// 7
	0b01111111,		// 8
	0b01101111,		// 9
	0b01110111,     // EDIT: A 
	0b01111100,		// EDIT: b
	0b00111001,		// EDIT: C
	0b01011110,		// EDIT: d
	0b01111001,		// EDIT: E
	0b01110001		// EDIT: F
};

//Number in: Go to 7 segmented display
void DisplayNumber(int number){
	number = (number > 16) ? number%16 : number;
	PORTB = numbers[number];
}

// main programm
int main2_3()
{
	DDRB=0xFF;						// poortB output
	
	while (1)
	{
		for (int i=0; i<=37; i++)		// show the numbers 0..F sequently
		{
			DisplayNumber(i);
			wait(3500);
		}
	}
	return 1;
} // end main

