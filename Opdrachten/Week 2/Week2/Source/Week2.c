/* Project name: 		Demo2_1 - LED_Blinking with interrupts
 * Author: 			Avans-TI, JW
 * Revision History:		20101217: - initial release;
 * Description:			This is a demo of input and ouput, it turns on/off pin0 or
	 				pin3 of PORTC, dependent of rising edge on input EX0 or EX1 
 * Test configuration:
     MCU:             	ATmega128
     Dev.Board:       	BIGAVR6
     Oscillator:      	External Clock 08.0000 MHz
     Ext. Modules:    	-
     SW:              	AVR-GCC
 * NOTES:				Turn ON the PORT LEDs at SW12.1 - SW12.8
*/
#include "7segDisplay.h"
#include "InterruptPractice.h"
#include "Lcd.h"
#include <util/delay.h>

void wait(int ms )
{
	for (int i=0; i<ms; i++)
	{
		_delay_ms( 1 );				// library function (max 30 ms at 8MHz)
	}
}

int main( void ){
	while(1){
		main2_4();
	}
	return 0;
}