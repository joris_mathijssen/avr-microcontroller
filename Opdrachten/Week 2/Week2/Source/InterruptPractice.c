/*
 * InterruptPractice.c
 *
 * Created: 12-2-2014 10:44:06
 *  Author: Kraegon
 */ 
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define BIT(x) (1 << (x));4
direction = 0;

unsigned char toggle = 1;

// Interrupt service routine External Input0
ISR( INT0_vect )
{
	direction = 0;
}

// Interrupt service routine External Input1
ISR( INT1_vect )
{
	direction = 1;
}

// Initialisation external interrupt EX0 and EX1
void exIntrInit( void )
{
	DDRD = 0x00;			// PE1 en PE2 for input
	EICRA |= 0x0F;			// EX0, EX1: rising edge
	EIMSK |= 0x03;			// turn_on EI0, EI1
	SREG |= 0x80;			// turn_on intr all
}


// Main program
int main2_2( void )
{
	DDRA=0xFF;					// PORTA is output
	DDRB=0xFF;					// PORTB too.

	exIntrInit();
	while (1)
	{ 						// DO forever
		int counter = 0;
		int lineNo = 0;
		if(lineNo == 0){
			PORTB = 0x00;
			PORTA = (1<<counter);
			} else if(lineNo == 1){
			PORTA = 0x00;
			PORTB = (1>>counter);
		}
		wait(400);
		counter++;
		if(counter == 8){
			counter = 0;
			lineNo++;
		}
	}// end while
	return 1;
}