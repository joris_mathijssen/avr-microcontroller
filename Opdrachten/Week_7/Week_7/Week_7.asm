; demoprogramma 6_3a:
; Demo voor toepassing subroutine, 16 bits optelling Add16
;
.INCLUDE "m128def.inc"
;
.CSEG
.ORG 		$0000
			rjmp main

; memory allocation for the stack at the end of dataram
.DSEG	
.ORG		$0f00
			.BYTE	256

;--------------------------------------------------------------------------------------------
; Each subroutine / function has its own code segment.
;--------------------------------------------------------------------------------------------
.CSEG 		
.ORG		$0200 				; 0200H is address for subroutine Add16
; 16 bits addition: 			; R5/R4 = R9/R8 + R7/R6
; 
Add16:		mov	r4, r8			; R4 = R6+R8 	
			add	r4, r6			; 
			mov	r5, r9			; R3 = R5+R6+C
			adc	r5, r7			;
			ret 


;--------------------------------------------------------------------------------------------
; Code segment function main()
;--------------------------------------------------------------------------------------------
.CSEG
.ORG		$0400
;
main:
; initialisatie stack pointer
			ldi		r16, $0f	; load stackpointer to 0x0fff (end of dataram)
			out		sph, r16	;
			ldi		r16, $ff	;
			out		spl, r16	;
;
; 16 bits optelling: R5/R4 = R9/R8 + R7/R6
			ldi		yh, $08		; Y <- $0800		adres in data memory
			ldi		yl, $00		;
			ldd		r8, Y+0		; Get values from data memory
			ldd		r9, Y+1		;
			ldd		r6, Y+2		;
			ldd		r7, Y+3		;
								; 
			call	Add16		; Call subroutine Add16
								;
			std		Y+4, r4		; Store result in data memory
			std		Y+5, r5		;
;
no_end:   					; unlimited loop, when done
		rjmp no_end			
		
.DSEG
.ORG	$0800
		.BYTE	6
