/*
 * Lcd.h
 *
 * Created: 6-2-2014 16:05:01
 *  Author: Kraegon
 */ 


#ifndef LCD_H_
#define LCD_H_

//Procedure methods to call from true main.
extern int main2_4(void);


//Important commands, too lazy to implement a procedural file like LcdDo
extern void lcdDefinePattern(unsigned char animation[]);
extern void lcdBlinkText(char text3[]);
extern void lcdWriteLine1( char text1[] );
extern void lcdWriteLine2( char text2[] );
extern void lcdShift(int displacement);
extern void lcdInit(void);

#endif /* LCD_H_ */