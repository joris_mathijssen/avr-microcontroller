/*
 * CounterT2.c
 *
 * Created: 13-2-2014 11:38:21
 *  Author: Kraegon
 */ 

/* 
 * Project name : Demo3_1 : Counting events met Counter/Timer 2 
 * Author : Avans-TI, JW 
 * Revision History : 20101222: - initial release; 
 * Description : This program counts event on pin T2 = PD7. 
 the result is in register TCNT2, shown on output port B 
 * Test configuration: 
 MCU : ATmega128 
 Dev.Board : BIGAVR6 
 Oscillator : External Clock 08.0000 MHz 
 Ext. Modules : - 
 SW : AVR-GCC 
 * NOTES: 
 - Turn ON the PORT LEDs at SW12.1 - SW12.8 
*/ 
 
#include <avr/io.h> 
#include <util/delay.h> 
#include <avr/interrupt.h>
#include "Lcd.h"

#define BIT(x) (1 << (x))
 
//COMMON PARAMETERS 
int interruptSource; //Enumerator to support our multiple main system. 
char* displayText;
//3_1 PARAMETERS 
int pressCount;
//3_2 PARAMETERS
int pulseCounter;
int twentyfifthcounter;
//3_3 PARAMETERS

ISR( TIMER2_OVF_vect )
{
	switch (interruptSource)
	{
		case 0:
			TCNT2 = -1;
			pressCount++;			// Increment counter
			sprintf(displayText, "%d", pressCount);
			lcdWriteLine1(displayText);
		break;
		case 1:
			TCNT2 = -1;	
			pulseCounter++;
			if(pulseCounter == 25){
				pulseCounter = 0;	
				twentyfifthcounter++;
			}
			sprintf(displayText, "%d | %d", twentyfifthcounter, pulseCounter);
			lcdWriteLine1(displayText);			
		break;		
		case 2:
			//3_3t
			
		break;
		default:
			sprintf(displayText, "Error %d", interruptSource);
			lcdWriteLine1(displayText);
		break;
	}
}
 
// Main program: Counting on T2 
int main3_1( void ) //Init
{ 
	interruptSource = 0;	//On interrupt: procedure 0
	DDRD = 0x00;			//Set all of port D to output except D7
	DDRB = 0xFF;			//Set all of port B to output
	DDRA = 0xFF;            //Set all of port A to output
	displayText = malloc(sizeof(char) * 16);
	displayText = "0";		//Initialise display text
	lcdWriteLine1(displayText);
	TCNT2 = -1;				// Preset value of counter 2
	TIMSK |= BIT(6);		// T2 overflow interrupt enable
	SREG |= BIT(7);			// turn on global interrupt
	TCCR2 = 0x07;			// Initialize T2: ext.counting, rising edge, run
	while(1){
		PORTB = TCNT2;		//Debug TCNT2 register
		wait(10);	
	}
} int main3_2( void ){	interruptSource = 1;		DDRD = 0x00;	DDRB = 0xFF;	DDRA = 0xFF;	displayText = malloc(sizeof(char) * 16);
	displayText = "0";
	lcdWriteLine1(displayText);	TCNT2 = -1;	TIMSK |= BIT(6);	SREG |= BIT(7);	TCCR2 = 0x07;		while(1){		wait(1500);		PORTA = TCNT2; 		PORTB = pulseCounter;	}}